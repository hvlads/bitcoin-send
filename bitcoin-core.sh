#!/bin/bash
docker run -v ${PWD}/data:/home/bitcoin/.bitcoin  --rm -it \
  -p 18443:18443 \
  -p 18444:18444 \
  ruimarinho/bitcoin-core \
  -printtoconsole \
  -regtest=1 \
  -rpcallowip=172.17.0.0/16 \
  -rpcbind=0.0.0.0 \
  -rpcauth='foo:35c817dee6031b5679da3e0d5e6b7ffe$a2398fe091f76d8969d9096bdfe4c0f33f7db133164757619e0548b2b05a3d21'

# pass LEfG5QJuFu6zx3SzkW-O2w4SN6pgMjCF_X6ny4oE7p0=