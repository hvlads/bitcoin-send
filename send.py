"""
Необходимо написать функцию сбора и отправки bitcoin транзакции на python 3,
используя апи кошелька bitcoin-core>=0.19.

На вход подается адрес получателя, сумма, список разрешенных,
запрещенных или приоритетных для использования выходов (utxo),

флаг о включение комиссии в отправляемую сумму.

Сложность в том, что стандартные апи методы sendtoaddress, sendmany не позволяют выбирать utxo,
из которых будет формироваться транзакция, поэтому придется использовать методы createpsbt, lockunspent,
либо какие-то другие ухищрения.

Используя метод createpsbt придется организовать работу со сдачей — на какие адреса её переводить и где их хранить.

Актуальную комиссию сети можно получить через методы bitcoin-core.

Возможно в новых версиях bitcoin-core реализованы вспомогательные методы,
которые помогут это с легкостью реализовать.


Прототип функции
def send(to, amount, whitelist=None, blacklist=[], includefee=False):
    return txid

Возможные ошибки
InsufficientFunds
WalletRPCError

Примеры использования

Допустим в кошельке есть несколько неизрасходованных выходов:
txid:vout amount
10001:1 0.5
10002:0 1.2
10003:3 0.3
10004:1 0.9
10005:2 0.1

1. Нам необходимо отправить 0.1, не используя выход 10003:3
Должна сформироваться оптимальная транзакция c одним из доступным выходов (10002:0), чтобы хватило комиссии сети,
а сдача уйти на новый адрес.

txid = send('bc123456', 0.1, blacklist=['10003:3'])

2. Необходимо отправить 0.9, не используя выход 10003:3
Должна сформироваться оптимальная транзакция c несколькими доступными выходами (например 10004:1, 10005:2),
чтобы хватило на комиссию сети, а сдача уйти на новый адрес.

txid = send('bc123456', 0.9, blacklist=['10003:3'])

3. Необходимо отправить 0.9, включая комиссию сети, строго используя выход 10004:1.
Транзакция должна использовать только один заданный выход и отправить на адрес bc123456 сумму amount=0.9-network_fee

txid = send('bc123456', 0.9, whitelist=['10004:1'], includefee=True)

4. Необходимо отправить 1.9, в приоритете используя выходы 10005:2 и 10003:3.
В первую очередь должны браться заданные выходы, потом браться другие,
чтобы было оптимально (например в данном случае 10001:1 и 10002:0), и отправить на адрес bc123456

txid = send('bc123456', 1.9, priority=['10005:2', '10003:3'])

"""
from pprint import pprint

from bit import PrivateKeyTestnet
from bit.network import get_fee


# node.importaddress(key.segwit_address, "optional-label", False)
# pprint(key.balance)
# pprint(key.balance_as('usd'))
# pprint(key.get_unspents()) # все выходы
# # print(key.to_wif())
# pprint(key.address) # muPPfnr9vMsS2DBiieAonHokS9QtLZcQyn
# # tx_hash = key.create_transaction([('muPPfnr9vMsS2DBiieAonHokS9QtLZcQyn', 1, 'usd')])
# # print('tx', tx_hash)
# print(key.get_balance('btc'))
# test Yet Another Bitcoin Testnet Faucet  https://testnet-faucet.mempool.co/
# check https://blockstream.info/testnet/search


class InsufficientFunds(Exception):
    def __str__(self):
        return 'Insufficient funds'


class WalletRPCError(Exception):
    def __str__(self):
        return 'Wallet RPC error'


user = 'foo'
password = 'LEfG5QJuFu6zx3SzkW-O2w4SN6pgMjCF_X6ny4oE7p0='
key = PrivateKeyTestnet('cV52bRPNHLWePWi9p9aikNjMpJS7t2KcEZDQy9RjgW4HKa8zNSqt')


# leftover - адрес куда должна уйти сдача

def send(to, amount, whitelist=None, blacklist=None, includefee=False, priority=None, leftover=None):
    fee = get_fee(fast=False) / 100000000
    if includefee:
        amount -= fee

    key.get_balance('btc')
    # Подключиться к удаленному узлу Bitcoin Core
    # https://ofek.dev/bit/guide/network.html
    # try:
    #     node = NetworkAPI.connect_to_node(user=user,
    #                                       password=password,
    #                                       host='127.0.0.1', port=18443,
    #                                       use_https=False,
    #                                       testnet=True)
    #     node.importaddress(key.segwit_address, "optional-label", False)
    # except Exception:
    #     raise WalletRPCError

    pprint(key.unspents)

    unspents = key.unspents
    key_unspents = key.unspents

    bad_unspents = None
    if whitelist:
        unspents = [x for x in unspents if x.txid not in whitelist]

    if blacklist:
        unspents = [x for x in unspents if x.txid not in blacklist]

    if priority:
        unspents = [x for x in unspents if x.txid in priority]
        bad_unspents = [x for x in unspents if x.txid not in priority]

    # print(key.unspents == unspents)

    balance = sum([x.amount for x in unspents]) / 100000000
    if balance < amount:
        if not priority:
            raise InsufficientFunds
        else:
            key_unspents = [x for x in key_unspents if x not in unspents]
            while key_unspents:
                unspents.append(key.unspents.pop())
                balance = sum([x.amount for x in unspents]) / 100000000
                if balance < amount:
                    continue
                else:
                    break
    balance = sum([x.amount for x in unspents]) / 100000000
    if balance < amount:
        if priority:
            insuffi_cient_funds = True
            while bad_unspents:
                if balance < amount:
                    unspents.push(bad_unspents.pop())
                else:
                    insuffi_cient_funds = False
                    break
            if insuffi_cient_funds:
                raise InsufficientFunds
        else:
            raise InsufficientFunds

    params = {
        'combine': False,
        'unspents': unspents
    }
    if leftover:
        params['leftover'] = leftover

    tx_id = key.send([(to, amount, 'btc')], **params)
    return tx_id


# test blacklist
s = send('muPPfnr9vMsS2DBiieAonHokS9QtLZcQyn', 0.001,
         blacklist=['7a5d741ffdc7164f7cb030709b2c5f35f93f62d7ced28035ce840f2ac7a9ded3', ])


# test InsufficientFunds Exception
# send('muPPfnr9vMsS2DBiieAonHokS9QtLZcQyn', 1000000000,
#      blacklist=['7a5d741ffdc7164f7cb030709b2c5f35f93f62d7ced28035ce840f2ac7a9ded3'])
